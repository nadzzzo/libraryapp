﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.ComponentModel;


namespace Osobe
{
   public abstract class Osoba
    {
        public string ime, prezime;
        public string maticniBroj;
        DateTime datumRodjenja;
        string password;
        public string username;
       
       
        public string CalculateMD5Hash(string input)
        {
            MD5 md5 = System.Security.Cryptography.MD5.Create();
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
            byte[] hash = md5.ComputeHash(inputBytes);
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X2"));
            }
            return sb.ToString();
        }

        public Osoba(string ime, string prezime, string JMBG, DateTime datumrodjenja,string username, string pass)
        {
            this.ime = ime;
            this.prezime = prezime;
            this.maticniBroj = JMBG;
            this.datumRodjenja = datumrodjenja;
            this.username = username;
            string pravi = CalculateMD5Hash(pass);
            this.password = pravi;
        }
        public bool ValidacijaMaticnog(string maticni, DateTime datumRodj)
        {
            if (maticni.Length != 13) return false;
            string dan = "" + maticni[0] + "" + maticni[1];
            string mjesec = "" + maticni[2] + "" + maticni[3];
            string godina = "1" + maticni[4] + "" + maticni[5] + "" + maticni[6];
            string datum = "" + dan + "/" + mjesec + "/" + godina;
            DateTime datumpravi = Convert.ToDateTime(datum);
            if (datumpravi == datumRodj) return true;
            return false;
        }
        public string DajIme() { return ime; }
        public string DajPrezime() { return prezime; }
        public string DajJMBG() { return maticniBroj;  }
        public DateTime DajDatumRodjenja() { return datumRodjenja; }
        public string DajUsername() { return username; }
        public string DajPassword() { return password; }
    
    

    }
}
