﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;


namespace Osobe
{
    public abstract class Clan: Osoba
    {
        int sifraClana;
       public bool mjesecnaClanarina;
       public bool godisnjaClanarina;
        string komentar;
        DateTime datumUclanjenja;
        List<int> iznajmljeneKnjige;
        bool validan;
        public double popust;
        Image slika;
        public DateTime DajDatumDoKadVrijediClanarina()
        {
            if (mjesecnaClanarina == true && godisnjaClanarina == false)

                return datumUclanjenja.AddDays(30);
          
                return datumUclanjenja.AddYears(1);
        }

        public string IspisiClana()
        {
            return "Ime: " + ime + "\n" + "Prezime: " + prezime + "\n" + "Username: " + username + "\n" + "Šifra: " + sifraClana;
        }

        public Clan(string ime, string prezime, string jmbg, DateTime datumrodjenja, int sifra, bool mjesecna, bool godisnja, string komentar, DateTime datumuclanjenja, string username, string pass)
            : base(ime, prezime, jmbg, datumrodjenja, username, pass)
        {
            this.sifraClana = sifra;
            this.komentar = komentar;
            this.mjesecnaClanarina = mjesecna;
            this.godisnjaClanarina = godisnja;
            this.datumUclanjenja = datumuclanjenja;
            this.validan = true;
            this.iznajmljeneKnjige = new List<int>();
        }

        public double DajPopust() { return popust; }
        public int DajSifruClana() { return sifraClana; }
        public string DajKomentar() { return komentar; }
        public bool DaLiJeGodisnja() { return godisnjaClanarina; }
        public bool DaLiJeMjesecna() { return mjesecnaClanarina; }
        public List<int> DajListuIznajmljenihKnjiga() { return iznajmljeneKnjige; }
        public void ObrisiSaListeIznajmljenihKnjiga(int sifra)
        {
            for(int i = 0; i<iznajmljeneKnjige.Count; i++)
            {
                if (iznajmljeneKnjige[i] == sifra) { iznajmljeneKnjige.Remove(sifra); return; }
            }
            throw new Exception();
        }
        public DateTime DajDatumUclanjenja() { return datumUclanjenja; }
   

        public void DodajNaListuIznajmljenihKnjiga(int sifra)
        {
            iznajmljeneKnjige.Add(sifra);
        }

     

        public bool DaLiJeClanValidan()
        {
            if (mjesecnaClanarina)
            {
                TimeSpan razlika = DateTime.Now - datumUclanjenja;
                var dani = razlika.TotalDays;
                if (dani > 30) validan = false;
            }
            else if (godisnjaClanarina)
            {
                TimeSpan razlika = DateTime.Now - datumUclanjenja;
                var dani = razlika.TotalDays;
                if (dani > 365) validan = false;
            }
            return validan;
        }
        public void PostaviKomentarClanu(string s)
        {
            this.komentar = s;
        }
    }
}
