﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Osobe
{
    public class Profesori : Clan
    {
        int sifraProfesora;

        public Profesori(string ime, string prezime, string jmbg, DateTime datumrodjenja, int sifra, bool mjesecna, bool godisnja, string komentar, DateTime datumuclanjenja, int sifraProfesora, string username, string pass)
            :
            base(ime, prezime, jmbg, datumrodjenja, sifra, mjesecna, godisnja, komentar, datumuclanjenja, username, pass)
        {
            this.popust = 0.15;
            this.sifraProfesora = sifraProfesora;
        }

        public int DajSifruProfesora() { return sifraProfesora; }
    }
}
