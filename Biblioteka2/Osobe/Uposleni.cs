﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Osobe
{
   public class Uposleni : Osoba
    {
        int sifraBibliotekara;
        bool admin;
        public int DajSifruBibliotekara() { return sifraBibliotekara; }

        public Uposleni(string ime, string prezime, string maticnibroj, DateTime datumrodjenja, int sifra, string username, string pass, bool admin) 
            : 
            base(ime, prezime, maticnibroj, datumrodjenja, username, pass)
        {
            sifraBibliotekara = sifra;
            this.admin = admin;
        }
        public bool DaLiJeAdmin() { return admin; }
        public string IspisiUposlenog()
        {
            return "Ime: " + ime + "\n" + "Prezime: " + prezime + "\n" + "Username: " + username + "\n" + "Šifra: " + sifraBibliotekara;
        }
    }
}
