﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Osobe
{
    public class MoE : Clan
    {
        int indeks;
         public MoE(string ime, string prezime, string jmbg, DateTime datumrodjenja, int sifra, bool mjesecna, bool godisnja, string komentar, DateTime datumuclanjenja,int indeks, string username, string pass)
            :
            base(ime, prezime, jmbg, datumrodjenja, sifra, mjesecna, godisnja, komentar, datumuclanjenja, username, pass)
        {
            this.popust = 0.12;
            this.indeks = indeks;
        }
        
        public int DajBrojIndeksaM() { return indeks; }
    }
}
