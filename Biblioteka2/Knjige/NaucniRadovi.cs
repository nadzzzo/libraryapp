﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Knjige
{
    public class NaucniRadovi: PredmetiCitanja
    {
       string konferencija;
       string oblastNauke;

        public NaucniRadovi(int sif, string ime, string autori, string znr, string izdavac, int godizd, string isbn, string konf, string oblast)
            :
            base(sif,ime,autori,znr,izdavac,godizd,isbn)
        {
            this.konferencija = konf;
            this.oblastNauke = oblast;
        }
        public string DajKonferenciju() { return konferencija; }
        public string DajOblastRada() { return oblastNauke; }
    }
}
