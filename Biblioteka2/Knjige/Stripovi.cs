﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Knjige
{
    public class Stripovi : PredmetiCitanja
    {
         string imeAutorskeKuce;
         List<string> spisakUmjetnika;
         int brojIzdanja;
         bool obicnoIzdanje;
        
        public Stripovi(int sif, string ime, string autori, string znr, string izdavac, int godizd, string isbn, string imekuce, List<string> spisakumj, int brizdanja, bool obicno)
            :
            base(sif,ime,autori,znr,izdavac,godizd,isbn)
        {
            this.imeAutorskeKuce = imekuce;
            this.spisakUmjetnika = spisakumj;
            this.brojIzdanja = brizdanja;
            this.obicnoIzdanje = obicno;
        }

        public string DajImeAutorskeKuce() { return imeAutorskeKuce; }
        public List<string> DajspisakUmjetnika() { return spisakUmjetnika; }
        public int DajBrojIzdanja() { return brojIzdanja; }
        public bool DaLiJeObicnoIzdanje() { return obicnoIzdanje; }


  
    }
}
