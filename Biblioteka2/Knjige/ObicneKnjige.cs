﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Knjige
{
    public class ObicneKnjige : PredmetiCitanja
    {
        public ObicneKnjige(int sif, string ime, string autori, string znr, string izdavac, int godizd, string isbn)
            :
            base(sif,ime,autori,znr,izdavac,godizd,isbn) { }

    }
}
