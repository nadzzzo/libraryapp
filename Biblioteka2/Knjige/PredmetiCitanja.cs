﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace Knjige
{
    public abstract class PredmetiCitanja
    {
        public int sifra;
        public string naslov;
        public string autor;
        public string zanr;
        public string nazivIzdavaca;
        public int godinaIzdanja;
        public string ISBN;
        public bool izdata;
        public Regex rgx;
        public int primjerak;
        string pattern = (@"^ISBN \d-\d{4}-\d{3}-\d{1}$");

        public PredmetiCitanja(int sif, string ime, string autori, string znr, string izdavac, int godizd, string isbn)
        {

            this.sifra = sif;
            this.naslov = ime;
            this.autor = autori;
            this.nazivIzdavaca = izdavac;
            this.godinaIzdanja = godizd;
            this.ISBN = isbn;
            this.izdata = false;
            this.primjerak = 1;
        }
        public string IspisiKnjigu()
        {
            return "Naslov: " + naslov + "\n" + "Autor " + autor + "\n" + "Izdavač: " + nazivIzdavaca + "\n" + "Godina izdanja: " + godinaIzdanja;
        }
        public  int DajSifruKnjige() { return sifra; }
        public  string DajNaslovKnjige() { return naslov; }
        public  string DajAutora() { return autor; }
        public  string DajZanr() { return zanr; }
        public  string DajNazivIzdavaca() { return nazivIzdavaca; }
        public  int DajGodinuIzdanja() { return godinaIzdanja; }
        public  string DajISBN() { return ISBN; }
        public  bool DaLiJeIzdata() { return izdata; }
        public  void IznajmiKnjigu() { izdata = true; primjerak--;}
        public void DodajKnjigu(int br) { primjerak += br; }
        public int DajBrojPrimjeraka() { return primjerak;  }
    }
}
