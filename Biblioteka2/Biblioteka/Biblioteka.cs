﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Osobe;
using Knjige;
using System.Security.Cryptography;
using System.Text.RegularExpressions;
namespace Biblioteka
{
    public class Biblioteka
    {
        int brojClanova;
        int brojKnjiga;
        int brojZaposlenih;
        List<PredmetiCitanja> ListaKnjiga;
        List<Clan> ListaClanova;
        List<Uposleni> ListaZaposlenih;
        List<Iznajmljivanje> ListaIznajmljenih;
        double stanje = 0;
        const int CijenaGodisnja = 50;
        const int CijenaMjesecna = 10;
        const int BrojDana = 7;

        public Biblioteka()
        {
            this.ListaKnjiga = new List<PredmetiCitanja>();
            this.ListaClanova = new List<Clan>();
            this.ListaZaposlenih = new List<Uposleni>();
            this.ListaIznajmljenih = new List<Iznajmljivanje>();

        }
       
         public bool ValidacijaMaticnog(string maticni, DateTime datumRodj)
        {
            if (maticni.Length != 13) return false;
            string dan = "" + maticni[0] + "" + maticni[1];
            string mjesec = "" + maticni[2] + "" + maticni[3];
            string godina = "1" + maticni[4] + "" + maticni[5] + "" + maticni[6];
            string datum = "" + dan + "/" + mjesec + "/" + godina;
            // DateTime datumpravi = Convert.ToDateTime(datum);
            DateTime datumpravi = new DateTime(Convert.ToInt32(godina), Convert.ToInt32(mjesec), Convert.ToInt32(dan));
            if (datumpravi.Date == datumRodj.Date) return true;
            return false;
        }

        public void VratiKnjigu(int sifraknjige, string username)
        {
            foreach(var C in ListaClanova)
            {
                if(C.DajUsername() == username)
                {
                    C.ObrisiSaListeIznajmljenihKnjiga(sifraknjige);
                    return;
                }
            }
            throw new Exception("Neuspješno!");
        }
    
        public bool ValidacijaISBN(string isbn)
        {   Regex rgx;
            string pattern = (@"^ISBN \d-\d{4}-\d{3}-\d{1}$");
            if (Regex.IsMatch(@isbn, pattern) == true) return true;
            return false;
        }
        public Clan PretraziClanaSaSifrom(int sif)
        {
            foreach(var K in ListaClanova)
            {
                if (K.DajSifruClana() == sif)
                    return K;
            }
            throw new Exception("Nije pronadjen");
        }
        public  Uposleni PretraziUposlenogSaSifrom(int sif)
        {
            foreach (var K in ListaZaposlenih)
            {
                if (K.DajSifruBibliotekara() == sif)
                    return K;
            }
            throw new Exception("Nije pronadjen");
        }
        public List<Clan> PretraziClanaSaImenom(string ime)
        {
            List<Clan> nova = new List<Clan>();
            foreach(var K in ListaClanova)
            {
                if(K.DajIme() == ime)
                {
                    nova.Add(K);
                }
                
            }
            if (nova.Count != 0) return nova;
            throw new Exception();
        }
        public List<Clan> PretraziClanaSaPrezimenom(string prezime)
        {
            List<Clan> nova = new List<Clan>();
            foreach(var K in ListaClanova)
            {
                if(K.DajPrezime() == prezime)
                {
                    nova.Add(K);
                }
             }
           if(nova.Count!=0)  return nova;
            throw new Exception();

        }
        public List<Uposleni> PretraziUposlenogSaPrezimenom(string prezime)
        {
            List<Uposleni> nova = new List<Uposleni>();
            foreach (var K in ListaZaposlenih)
            {
                if (K.DajPrezime() == prezime)
                {
                    nova.Add(K);
                }

            }
           if(nova.Count !=0) return nova;
            throw new Exception();

        }
        public Uposleni PretraziUposlenogSaUsernameom(string pojam)
        {
            foreach(var K in ListaZaposlenih)
            {
                if (K.DajUsername() == pojam)
                    return K;
            }
            throw new Exception("Nije pronađen");
        }
        public Clan PretraziClanaSaUsernemom(string user)
        {
            foreach (var K in ListaClanova)
            {
                if (K.DajUsername() == user)
                    return K;
            }
            throw new Exception("Nije pronadjen");

        }
       

        public List<PredmetiCitanja> PretraziKnjiguSaImenom(string ime)
        {
            List<PredmetiCitanja> nova = new List<PredmetiCitanja>();
            foreach (var K in ListaKnjiga)
            {
                if (K.DajNaslovKnjige() == ime)
                    nova.Add(K);
            }
            if (nova.Count != 0) return nova;
            throw new Exception();
        }
        public List<PredmetiCitanja> PretraziKnjiguSaAutorom(string ime)
        {
            List<PredmetiCitanja> nova = new List<PredmetiCitanja>();
            foreach (var K in ListaKnjiga)
            {
                if (K.DajAutora() == ime)
                    nova.Add(K);
            }
            if(nova.Count!= 0) return nova;
            throw new Exception();
        }
        public PredmetiCitanja PretraziKnjiguSaSifrom(int sif)
        {
            foreach (var K in ListaKnjiga)
            {
                if (K.DajSifruKnjige() == sif)
                    return K;
            }
            throw new Exception("Nije pronadjen");
        }
        public PredmetiCitanja PretraziKnjiguSaISBNSifrom(string sif)
        {
            foreach (var K in ListaKnjiga)
            {
                if (K.DajISBN() == sif)
                    return K;
            }
            throw new Exception("Nije pronadjen");
        }
        public void DodajNaStanje(double c)
        {
            stanje += c;
        }
        public double DajStanje()
        {
            return stanje;
        }
        public void RegistrujObicnuKnjigu(string naslov, int sifra, string zanr, string spisakAutora, string nazivIzdavaca, int godinaIzdanja, string isbn)
        {
            ObicneKnjige k = new ObicneKnjige(sifra, naslov, spisakAutora, zanr, nazivIzdavaca, godinaIzdanja, isbn);
            ListaKnjiga.Add(k);
            brojKnjiga++;
        }
        public void RegistrujKnjigu(PredmetiCitanja k)
        {
            ListaKnjiga.Add(k);
            brojKnjiga++;
        }
        public void RegistrujUposlenog(Uposleni O)
        {
            ListaZaposlenih.Add(O);
            brojZaposlenih++;

        }
        public void DodajIznajmljivanje(Iznajmljivanje i)
        {
            ListaIznajmljenih.Add(i);
        }
        public void RegistrujClana(Clan c,DateTime datum)
        {
            
            ListaClanova.Add(c);
            brojClanova++;
            double cijena = 0;
            if (c is ObicniClan)
            {
                if (c.DaLiJeGodisnja() == true) cijena = CijenaGodisnja - CijenaGodisnja * c.DajPopust();
                else if (c.DaLiJeMjesecna() == true) cijena = CijenaMjesecna - CijenaMjesecna * c.DajPopust();
            }
            else if (c is MoE)
            {
                if (c.DaLiJeGodisnja() == true) cijena = CijenaGodisnja - CijenaGodisnja * c.DajPopust();
                else if (c.DaLiJeMjesecna() == true) cijena = CijenaMjesecna - CijenaMjesecna * c.DajPopust();

            }
            else if (c is BoE)
            {
                if (c.DaLiJeGodisnja() == true) cijena = CijenaGodisnja - CijenaGodisnja * c.DajPopust();
                else if (c.DaLiJeMjesecna() == true) cijena = CijenaMjesecna - CijenaMjesecna * c.DajPopust();

            }
            else if (c is Profesori)
            {
                if (c.DaLiJeGodisnja() == true) cijena = CijenaGodisnja - CijenaGodisnja * c.DajPopust();
                else if (c.DaLiJeMjesecna() == true) cijena = CijenaMjesecna - CijenaMjesecna * c.DajPopust();
            }

            DodajNaStanje(cijena);
        }
        public void RegistrujStrip(string naslov, int sifra, string zanr, string spisakAutora, string nazivIzdavaca, int godinaIzdanja, string isbn, string imeAutorskeKuce, List<string> spisakUmjetnika, int brojIzdanja, bool obicno)
        {

            Stripovi strip = new Stripovi(sifra, naslov, spisakAutora, zanr, nazivIzdavaca, godinaIzdanja, isbn, imeAutorskeKuce, spisakUmjetnika, brojIzdanja, obicno);
            ListaKnjiga.Add(strip);
            brojKnjiga++;
        }
        public void RegistrujNaucniRad(string naslov, int sifra, string zanr, string autori, string nazivIzdavaca, int godinaIzdanja, string isbn, string konferencija, string oblastNauke)
        {

            NaucniRadovi naucniRad = new NaucniRadovi(sifra, naslov, autori, zanr, nazivIzdavaca, godinaIzdanja, isbn, konferencija, oblastNauke);
            brojKnjiga++;
        }

        public string CalculateMD5Hash(string input)
        {
            MD5 md5 = System.Security.Cryptography.MD5.Create();
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
            byte[] hash = md5.ComputeHash(inputBytes);
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X2"));
            }
            return sb.ToString();
        }
        public List<Clan> PretragaClanova(string ime)
        {
            Regex trazi = new Regex(ime);

            List<Clan> NadjeniClanovi = new List<Clan>();
            foreach (var C in ListaClanova)
            {
                if (trazi.IsMatch(C.DajIme()) || trazi.IsMatch(C.DajPrezime()))
                {
                    NadjeniClanovi.Add(C);
                }
            }
            return NadjeniClanovi;
        }
        public DateTime DatumUclanjenja(Clan c)
        {
            foreach(var C in ListaClanova)
            {
                if(C == c)
                {
                    return c.DajDatumDoKadVrijediClanarina();
                }
            }
            throw new Exception("ff");
        }
        public List<Uposleni> PretraziUposlenogSaImenom(string ime)
        {
            List<Uposleni> nova = new List<Uposleni>();
            foreach(var k in ListaZaposlenih)
            {
                if(k.DajIme() == ime)
                {
                    nova.Add(k);
                }
            }
           if(nova.Count !=0) return nova;
            throw new Exception();
        }
        public List<PredmetiCitanja> PretragaKnjiga(string ime)
        {
            List<PredmetiCitanja> NadjeneKnjige = new List<PredmetiCitanja>();
            Regex trazeni = new Regex(ime);

            foreach (var K in ListaKnjiga)
            {


                if (trazeni.IsMatch(K.DajNaslovKnjige()) || trazeni.IsMatch(K.DajNazivIzdavaca()) || trazeni.IsMatch(K.DajAutora()))
                {
                    NadjeneKnjige.Add(K);
                }
            }
            return NadjeneKnjige;
        }

        public void ObrisiClanaSaUsernemom(string user)
        {
            foreach (var U in ListaClanova)
            {
                if (U.DajUsername() == user)
                { ListaClanova.Remove(U); brojClanova--; return; }
            }
            throw new Exception("Nije pronađen");
        }
        public void ObrisiKnjiguSaSifrom(int sifra)
        {
            foreach(var K in ListaKnjiga)
            {
                if(K.DajSifruKnjige() == sifra)
                {
                    ListaKnjiga.Remove(K);
                    brojKnjiga--;
                    return;
                }
            }
            throw new Exception("Nije pronađena");
        }
        public void ObrisiUposlenogSaSifrom(int sifra)
        {
            foreach(var K in ListaZaposlenih)
            {
                if(K.DajSifruBibliotekara() == sifra)
                {
                    ListaZaposlenih.Remove(K);
                    return;
                }
            }
            throw new Exception("Nije pronađen");
        }
        public Clan NadjiClana(string username)
        {
            foreach (var C in ListaClanova)
            {
                if (C.DajUsername() == username)
                {
                    return C;
                }
            }
            throw new Exception("e");
        }

        public Uposleni NadjiUposlenog(string username)
        {
            foreach(var U in ListaZaposlenih)
            {
                if(U.DajUsername() == username)
                {
                    return U;
                }
            }
            throw new Exception("e");
        }

        public bool DaLiJeAdmin(string username, string pass)
        {
            foreach (var A in ListaZaposlenih)
            {
                if (A.DajPassword() == pass && A.DajUsername() == username && A.DaLiJeAdmin() == true)
                    return true;
            }
            return false;
        }

       /* public bool NadjiUposlenog(string username, string pass)
        {
            foreach (var C in ListaZaposlenih)
            {
                if (C.DajUsername() == username && C.DajPassword() == pass)
                {
                    return true;
                }
            }
            return false;
        }

    */

        public bool DaLiJeDostupna(int sifra)
        {
            foreach (var K in ListaKnjiga)
            {
                if (K.DajSifruKnjige() == sifra && K.DaLiJeIzdata() == false)
                    return true;
            }
            return false;
        }

        public List<PredmetiCitanja> DajIznajmljeneKnjige(Clan c)
        {

            List<int> knjige = c.DajListuIznajmljenihKnjiga();
            List<PredmetiCitanja> povratna = new List<PredmetiCitanja>();
            int duz = knjige.Count();
            for (int i = 0; i < duz; i++)
                povratna.Add(NadjiKnjiguSaSifrom(knjige[i]));
            return povratna;
        }

        public PredmetiCitanja NadjiKnjiguSaSifrom(int sifra)
        {
            foreach (var K in ListaKnjiga)
            {
                if (K.DajSifruKnjige() == sifra)
                    return K;
            }
            throw new Exception("e");
        }
        public PredmetiCitanja NadjiKnjiguSaImenom(string s)
        {
            foreach(var K in ListaKnjiga)
            {
                if(K.DajNaslovKnjige() + " - " + K.DajAutora() == s)
                {
                    return K;
                }
            }
            throw new Exception("S");
        }
        public void IznajmiKnjiguClanu(PredmetiCitanja knjiga, Clan clan, DateTime datum)
        {
            
            Iznajmljivanje iznajmljenaknjiga = new Iznajmljivanje(clan, knjiga, datum);
            knjiga.IznajmiKnjigu();
            clan.DodajNaListuIznajmljenihKnjiga(knjiga.DajSifruKnjige());
            ListaIznajmljenih.Add(iznajmljenaknjiga);
            
        }
        public void PromijeniStanjeKnjizi(int sifraknjige, int brojprimjeraka)
        {
            foreach (var K in ListaKnjiga)
            {
                if (K.DajSifruKnjige() == sifraknjige)
                {
                    K.DodajKnjigu(brojprimjeraka);
                    return;
                }
                    
            }
            throw new Exception("Nije nadjena");
        }

        public DateTime DatumIznajmljivanja(Clan c)
        {
            foreach(var C in ListaIznajmljenih)
            {
                if(c == C.DajClanaIznajmljivanja())
                {
                    return C.DajDatumIznajmljivanja();
                }
            }
            throw new Exception("greska");
        }

        public List<PredmetiCitanja> PretraziKnjige(string s)
        {
            List<PredmetiCitanja> lista = new List<PredmetiCitanja>();
            foreach (var K in ListaKnjiga)
            {
                if (K.DajAutora().Contains(s) || K.DajNaslovKnjige().Contains(s))
                {
                    lista.Add(K);
                }
            }
            return lista;
        }

        public PredmetiCitanja NadjiKnjiguSImenom(string s)
        {
           
            foreach(var K in ListaKnjiga)
            {
                if (K.DajNaslovKnjige() == s)
                    return K;
            }
            throw new Exception("Knjiga nije nađena!");
        }

        public List<PredmetiCitanja> DajSveKnjige()
        {
            return ListaKnjiga;
        }

    }
}
