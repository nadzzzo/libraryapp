﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Biblioteka
{
    public partial class FormFinansije : Form
    {
        Biblioteka bibl;
        public FormFinansije(Biblioteka b)
        {
            InitializeComponent();
            bibl = b;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            double km = bibl.DajStanje();
            textBox1.Visible = true;
            textBox1.Text = km + "KM";
            System.Drawing.Graphics g;
            g = panel1.CreateGraphics();
            int y = Convert.ToInt32(km);
            
            Pen mojPen = new Pen(System.Drawing.Color.Black, 1);
       
            SolidBrush mojBrush = new SolidBrush(System.Drawing.Color.White);
            // Crtanje osa x i y 
                g.DrawLine(mojPen, 30, 20, 330, 20);
            //mojGrafickiObjekat.DrawString("1000", Font, new SolidBrush(Color.Black), 5, 15);
            g.DrawLine(mojPen, 30, 60, 330, 60);
           // mojGrafickiObjekat.DrawString("900", Font, new SolidBrush(Color.Black), 5, 55);
              g.DrawLine(mojPen, 30, 100, 330, 100);
           // mojGrafickiObjekat.DrawString("300", Font, new SolidBrush(Color.Black), 7, 95);
           g.DrawLine(mojPen, 30, 140, 330, 140);
           // mojGrafickiObjekat.DrawString("100", Font, new SolidBrush(Color.Black), 7, 135);
            g.DrawLine(mojPen, 30, 180, 330, 180);
           // mojGrafickiObjekat.DrawString("0", Font, new SolidBrush(Color.Black), 7, 175);
           g.DrawLine(mojPen, 30, 180, 30, 20);
            
            g.FillRectangle(mojBrush, 50, 130, 50, 200);
        }

        private void FormFinansije_Load(object sender, EventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
