﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Knjige;
using Osobe;

namespace Biblioteka
{
    public class Iznajmljivanje
    {
        Clan clan;
        PredmetiCitanja knjiga;
        DateTime datum;

        public Iznajmljivanje(Clan c, PredmetiCitanja knjiga, DateTime datum)
        {
            this.clan = c;
            this.knjiga = knjiga;
            this.datum = datum;
        }
        public Clan DajClanaIznajmljivanja() { return clan; }
        public PredmetiCitanja DajIznajmljenuKnjigu() { return knjiga; }
        public DateTime DajDatumIznajmljivanja() { return datum; }
    }
}
