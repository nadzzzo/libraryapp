﻿namespace Biblioteka
{
    partial class FormPocetna
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormPocetna));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.pomoćToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.adresaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.telefonToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.emailToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.radnoVrijemeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pomoćToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.članToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listaŽeljaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.iznajmljeneKnjigeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.istekRokaČlanarineToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.istekRokaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pretragaKnjigaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.uposlenikToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.prikazSvihKnjigaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.provjeraStanjaKnjigeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.iznajmljivanjeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pretragaČlanovaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.promjenaStanjaKnjigeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.donacijeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.errorProvider2 = new System.Windows.Forms.ErrorProvider(this.components);
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider2)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.DarkBlue;
            this.label1.Font = new System.Drawing.Font("Palatino Linotype", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label1.Location = new System.Drawing.Point(19, 178);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(113, 26);
            this.label1.TabIndex = 0;
            this.label1.Text = "Username: ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Palatino Linotype", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label2.Location = new System.Drawing.Point(19, 210);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(101, 26);
            this.label2.TabIndex = 1;
            this.label2.Text = "Password:";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(141, 178);
            this.textBox1.Margin = new System.Windows.Forms.Padding(4);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(170, 24);
            this.textBox1.TabIndex = 2;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            this.textBox1.Validated += new System.EventHandler(this.textBox1_Validated);
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(141, 214);
            this.textBox2.Margin = new System.Windows.Forms.Padding(4);
            this.textBox2.Name = "textBox2";
            this.textBox2.PasswordChar = '*';
            this.textBox2.Size = new System.Drawing.Size(170, 24);
            this.textBox2.TabIndex = 3;
            this.textBox2.Validated += new System.EventHandler(this.textBox2_Validated);
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button1.Location = new System.Drawing.Point(128, 258);
            this.button1.Margin = new System.Windows.Forms.Padding(4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(100, 34);
            this.button1.TabIndex = 4;
            this.button1.Text = "Prijava";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            this.button1.Validated += new System.EventHandler(this.button1_Validated);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Font = new System.Drawing.Font("Palatino Linotype", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tabControl1.Location = new System.Drawing.Point(-2, -3);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(409, 379);
            this.tabControl1.TabIndex = 6;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.DarkBlue;
            this.tabPage1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.textBox1);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.button1);
            this.tabPage1.Controls.Add(this.textBox2);
            this.tabPage1.Location = new System.Drawing.Point(4, 26);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(401, 349);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Prijava";
            this.tabPage1.Click += new System.EventHandler(this.tabPage1_Click);
            this.tabPage1.Paint += new System.Windows.Forms.PaintEventHandler(this.tabPage1_Paint);
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.DarkBlue;
            this.tabPage2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.tabPage2.Controls.Add(this.richTextBox1);
            this.tabPage2.Controls.Add(this.menuStrip1);
            this.tabPage2.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.tabPage2.Location = new System.Drawing.Point(4, 26);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(401, 349);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Informacije";
            // 
            // richTextBox1
            // 
            this.richTextBox1.BackColor = System.Drawing.Color.DarkBlue;
            this.richTextBox1.Font = new System.Drawing.Font("Palatino Linotype", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox1.ForeColor = System.Drawing.SystemColors.Window;
            this.richTextBox1.Location = new System.Drawing.Point(3, 62);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.ReadOnly = true;
            this.richTextBox1.Size = new System.Drawing.Size(391, 137);
            this.richTextBox1.TabIndex = 1;
            this.richTextBox1.Text = "";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Font = new System.Drawing.Font("Palatino Linotype", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pomoćToolStripMenuItem,
            this.pomoćToolStripMenuItem1,
            this.donacijeToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(3, 3);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(391, 25);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // pomoćToolStripMenuItem
            // 
            this.pomoćToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.adresaToolStripMenuItem,
            this.telefonToolStripMenuItem,
            this.emailToolStripMenuItem,
            this.radnoVrijemeToolStripMenuItem});
            this.pomoćToolStripMenuItem.Name = "pomoćToolStripMenuItem";
            this.pomoćToolStripMenuItem.Size = new System.Drawing.Size(63, 21);
            this.pomoćToolStripMenuItem.Text = "Kontakt";
            this.pomoćToolStripMenuItem.Click += new System.EventHandler(this.pomoćToolStripMenuItem_Click);
            // 
            // adresaToolStripMenuItem
            // 
            this.adresaToolStripMenuItem.Name = "adresaToolStripMenuItem";
            this.adresaToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
            this.adresaToolStripMenuItem.Text = "Adresa";
            this.adresaToolStripMenuItem.Click += new System.EventHandler(this.adresaToolStripMenuItem_Click);
            // 
            // telefonToolStripMenuItem
            // 
            this.telefonToolStripMenuItem.Name = "telefonToolStripMenuItem";
            this.telefonToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
            this.telefonToolStripMenuItem.Text = "Telefon";
            this.telefonToolStripMenuItem.Click += new System.EventHandler(this.telefonToolStripMenuItem_Click);
            // 
            // emailToolStripMenuItem
            // 
            this.emailToolStripMenuItem.Name = "emailToolStripMenuItem";
            this.emailToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
            this.emailToolStripMenuItem.Text = "E-mail";
            this.emailToolStripMenuItem.Click += new System.EventHandler(this.emailToolStripMenuItem_Click);
            // 
            // radnoVrijemeToolStripMenuItem
            // 
            this.radnoVrijemeToolStripMenuItem.Name = "radnoVrijemeToolStripMenuItem";
            this.radnoVrijemeToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
            this.radnoVrijemeToolStripMenuItem.Text = "Radno vrijeme";
            this.radnoVrijemeToolStripMenuItem.Click += new System.EventHandler(this.radnoVrijemeToolStripMenuItem_Click);
            // 
            // pomoćToolStripMenuItem1
            // 
            this.pomoćToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.članToolStripMenuItem,
            this.uposlenikToolStripMenuItem});
            this.pomoćToolStripMenuItem1.Name = "pomoćToolStripMenuItem1";
            this.pomoćToolStripMenuItem1.Size = new System.Drawing.Size(58, 21);
            this.pomoćToolStripMenuItem1.Text = "Pomoć";
            // 
            // članToolStripMenuItem
            // 
            this.članToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.listaŽeljaToolStripMenuItem,
            this.iznajmljeneKnjigeToolStripMenuItem,
            this.istekRokaČlanarineToolStripMenuItem,
            this.istekRokaToolStripMenuItem,
            this.pretragaKnjigaToolStripMenuItem});
            this.članToolStripMenuItem.Name = "članToolStripMenuItem";
            this.članToolStripMenuItem.Size = new System.Drawing.Size(131, 22);
            this.članToolStripMenuItem.Text = "Član";
            this.članToolStripMenuItem.Click += new System.EventHandler(this.članToolStripMenuItem_Click);
            // 
            // listaŽeljaToolStripMenuItem
            // 
            this.listaŽeljaToolStripMenuItem.Name = "listaŽeljaToolStripMenuItem";
            this.listaŽeljaToolStripMenuItem.Size = new System.Drawing.Size(236, 22);
            this.listaŽeljaToolStripMenuItem.Text = "Lista želja";
            this.listaŽeljaToolStripMenuItem.Click += new System.EventHandler(this.listaŽeljaToolStripMenuItem_Click);
            // 
            // iznajmljeneKnjigeToolStripMenuItem
            // 
            this.iznajmljeneKnjigeToolStripMenuItem.Name = "iznajmljeneKnjigeToolStripMenuItem";
            this.iznajmljeneKnjigeToolStripMenuItem.Size = new System.Drawing.Size(236, 22);
            this.iznajmljeneKnjigeToolStripMenuItem.Text = "Iznajmljene knjige";
            this.iznajmljeneKnjigeToolStripMenuItem.Click += new System.EventHandler(this.iznajmljeneKnjigeToolStripMenuItem_Click);
            // 
            // istekRokaČlanarineToolStripMenuItem
            // 
            this.istekRokaČlanarineToolStripMenuItem.Name = "istekRokaČlanarineToolStripMenuItem";
            this.istekRokaČlanarineToolStripMenuItem.Size = new System.Drawing.Size(236, 22);
            this.istekRokaČlanarineToolStripMenuItem.Text = "Istek roka članarine";
            this.istekRokaČlanarineToolStripMenuItem.Click += new System.EventHandler(this.istekRokaČlanarineToolStripMenuItem_Click);
            // 
            // istekRokaToolStripMenuItem
            // 
            this.istekRokaToolStripMenuItem.Name = "istekRokaToolStripMenuItem";
            this.istekRokaToolStripMenuItem.Size = new System.Drawing.Size(236, 22);
            this.istekRokaToolStripMenuItem.Text = "Istek roka za vraćanje knjige";
            this.istekRokaToolStripMenuItem.Click += new System.EventHandler(this.istekRokaToolStripMenuItem_Click);
            // 
            // pretragaKnjigaToolStripMenuItem
            // 
            this.pretragaKnjigaToolStripMenuItem.Name = "pretragaKnjigaToolStripMenuItem";
            this.pretragaKnjigaToolStripMenuItem.Size = new System.Drawing.Size(236, 22);
            this.pretragaKnjigaToolStripMenuItem.Text = "Pretraga knjiga";
            this.pretragaKnjigaToolStripMenuItem.Click += new System.EventHandler(this.pretragaKnjigaToolStripMenuItem_Click);
            // 
            // uposlenikToolStripMenuItem
            // 
            this.uposlenikToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.prikazSvihKnjigaToolStripMenuItem,
            this.provjeraStanjaKnjigeToolStripMenuItem,
            this.iznajmljivanjeToolStripMenuItem,
            this.pretragaČlanovaToolStripMenuItem,
            this.promjenaStanjaKnjigeToolStripMenuItem});
            this.uposlenikToolStripMenuItem.Name = "uposlenikToolStripMenuItem";
            this.uposlenikToolStripMenuItem.Size = new System.Drawing.Size(131, 22);
            this.uposlenikToolStripMenuItem.Text = "Uposlenik";
            // 
            // prikazSvihKnjigaToolStripMenuItem
            // 
            this.prikazSvihKnjigaToolStripMenuItem.Name = "prikazSvihKnjigaToolStripMenuItem";
            this.prikazSvihKnjigaToolStripMenuItem.Size = new System.Drawing.Size(203, 22);
            this.prikazSvihKnjigaToolStripMenuItem.Text = "Prikaz svih knjiga";
            this.prikazSvihKnjigaToolStripMenuItem.Click += new System.EventHandler(this.prikazSvihKnjigaToolStripMenuItem_Click);
            // 
            // provjeraStanjaKnjigeToolStripMenuItem
            // 
            this.provjeraStanjaKnjigeToolStripMenuItem.Name = "provjeraStanjaKnjigeToolStripMenuItem";
            this.provjeraStanjaKnjigeToolStripMenuItem.Size = new System.Drawing.Size(203, 22);
            this.provjeraStanjaKnjigeToolStripMenuItem.Text = "Provjera stanja knjige";
            this.provjeraStanjaKnjigeToolStripMenuItem.Click += new System.EventHandler(this.provjeraStanjaKnjigeToolStripMenuItem_Click);
            // 
            // iznajmljivanjeToolStripMenuItem
            // 
            this.iznajmljivanjeToolStripMenuItem.Name = "iznajmljivanjeToolStripMenuItem";
            this.iznajmljivanjeToolStripMenuItem.Size = new System.Drawing.Size(203, 22);
            this.iznajmljivanjeToolStripMenuItem.Text = "Iznajmljivanje";
            this.iznajmljivanjeToolStripMenuItem.Click += new System.EventHandler(this.iznajmljivanjeToolStripMenuItem_Click);
            // 
            // pretragaČlanovaToolStripMenuItem
            // 
            this.pretragaČlanovaToolStripMenuItem.Name = "pretragaČlanovaToolStripMenuItem";
            this.pretragaČlanovaToolStripMenuItem.Size = new System.Drawing.Size(203, 22);
            this.pretragaČlanovaToolStripMenuItem.Text = "Pretraga članova";
            this.pretragaČlanovaToolStripMenuItem.Click += new System.EventHandler(this.pretragaČlanovaToolStripMenuItem_Click);
            // 
            // promjenaStanjaKnjigeToolStripMenuItem
            // 
            this.promjenaStanjaKnjigeToolStripMenuItem.Name = "promjenaStanjaKnjigeToolStripMenuItem";
            this.promjenaStanjaKnjigeToolStripMenuItem.Size = new System.Drawing.Size(203, 22);
            this.promjenaStanjaKnjigeToolStripMenuItem.Text = "Promjena stanja knjige";
            this.promjenaStanjaKnjigeToolStripMenuItem.Click += new System.EventHandler(this.promjenaStanjaKnjigeToolStripMenuItem_Click);
            // 
            // donacijeToolStripMenuItem
            // 
            this.donacijeToolStripMenuItem.Name = "donacijeToolStripMenuItem";
            this.donacijeToolStripMenuItem.Size = new System.Drawing.Size(69, 21);
            this.donacijeToolStripMenuItem.Text = "Donacije";
            this.donacijeToolStripMenuItem.Click += new System.EventHandler(this.donacijeToolStripMenuItem_Click);
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // errorProvider2
            // 
            this.errorProvider2.ContainerControl = this;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 351);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(405, 22);
            this.statusStrip1.TabIndex = 7;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.ActiveLinkColor = System.Drawing.Color.Red;
            this.toolStripStatusLabel1.BackColor = System.Drawing.SystemColors.MenuBar;
            this.toolStripStatusLabel1.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.toolStripStatusLabel1.ForeColor = System.Drawing.Color.Red;
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(254, 22);
            this.toolStripStatusLabel1.Text = "Pogrešan username ili password!";
            this.toolStripStatusLabel1.Visible = false;
            // 
            // FormPocetna
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkBlue;
            this.ClientSize = new System.Drawing.Size(405, 373);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.tabControl1);
            this.Font = new System.Drawing.Font("Segoe Script", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "FormPocetna";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Početna";
            this.Load += new System.EventHandler(this.Pocetna_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider2)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem pomoćToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pomoćToolStripMenuItem1;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.ToolStripMenuItem adresaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem telefonToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem emailToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem članToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem uposlenikToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem radnoVrijemeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem donacijeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem listaŽeljaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem iznajmljeneKnjigeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem istekRokaČlanarineToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem istekRokaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pretragaKnjigaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem prikazSvihKnjigaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem provjeraStanjaKnjigeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem iznajmljivanjeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pretragaČlanovaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem promjenaStanjaKnjigeToolStripMenuItem;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private System.Windows.Forms.ErrorProvider errorProvider2;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
    }
}