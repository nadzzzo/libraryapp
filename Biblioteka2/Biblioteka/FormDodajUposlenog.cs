﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Osobe;

namespace Biblioteka
{
    public partial class FormDodajUposlenog : Form
    {
        Biblioteka biblioteka;
        public FormDodajUposlenog(Biblioteka b)
        {
            InitializeComponent();
            biblioteka = b;
        }

        private void FormDodajUposlenog_Load(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string ime = textBox1.Text;
            string prezime = textBox2.Text;
            DateTime datumrodjenja = dateTimePicker1.Value;
            string maticni = maskedTextBox1.Text;
            string username = textBox4.Text;
            string pass = textBox5.Text;
            int sifra = Convert.ToInt32(textBox6.Text);
            try
            {
                Uposleni uposlenik = new Uposleni(ime, prezime, maticni, datumrodjenja, sifra, username, pass, false);
                MessageBox.Show("Uspješno ste registrovali uposlenika!");
            }
            catch(Exception izuzetak)
            {

            }
        }

        private void textBox1_Validated(object sender, EventArgs e)
        {
            if (textBox1.Text == "") errorProvider1.SetError(this.textBox1, "Nije uneseno ime");
            else errorProvider1.SetError(this.textBox1, String.Empty);
        }

        private void textBox2_Validated(object sender, EventArgs e)
        {
            if (textBox2.Text == "") errorProvider2.SetError(this.textBox2, "Nije uneseno ime");
            else errorProvider2.SetError(this.textBox2, String.Empty);
        }

        private void textBox3_Validated(object sender, EventArgs e)
        {

        }

        private void maskedTextBox1_Validated(object sender, EventArgs e)
        {
            errorProvider3.Clear();
            if (maskedTextBox1.Text == "" || biblioteka.ValidacijaMaticnog(maskedTextBox1.Text, dateTimePicker1.Value) == false)

            {
                errorProvider3.SetError(this.maskedTextBox1, "Neispravan matični broj!");

            }
            else errorProvider3.SetError(this.textBox1, String.Empty);
        }

        private void textBox4_Validated(object sender, EventArgs e)
        {
            if (textBox4.Text == "") errorProvider4.SetError(this.textBox4, "Nije unesen username");
            else errorProvider4.SetError(this.textBox4, String.Empty);

        }

        private void textBox5_Validated(object sender, EventArgs e)
        {
            if (textBox5.Text == "") errorProvider5.SetError(this.textBox5, "Nije unesen password");
            else errorProvider5.SetError(this.textBox5, String.Empty);
        }

        private void textBox6_TextChanged(object sender, EventArgs e)
        {
            if (textBox6.Text == "") errorProvider6.SetError(this.textBox6, "Nije unesena šifra");
            else errorProvider6.SetError(this.textBox6, String.Empty);
        }

        private void textBox7_Validating(object sender, CancelEventArgs e)
        {

        }

        private void textBox7_Validated(object sender, EventArgs e)
        {
            if (textBox7.Text == "")
                errorProvider7.SetError(this.textBox7, "Nije unesen password!");
            else if (textBox7.Text != "" && textBox7.Text != textBox5.Text)
                errorProvider7.SetError(this.textBox7, "Password se ne poklapa!");
            else errorProvider7.SetError(this.textBox7, String.Empty);
        }
    }
}
