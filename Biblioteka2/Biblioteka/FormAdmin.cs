﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Osobe;
using Biblioteka;
using Knjige;
namespace Biblioteka
{
    public partial class FormAdmin : Form
    {
        Biblioteka bibl;
        public FormAdmin(Biblioteka b)
        {
            InitializeComponent();
            bibl = b;    
        }


        private void Form4_Load(object sender, EventArgs e)
        {

        }

        private void iznosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormFinansije nova = new FormFinansije(bibl);
            nova.ShowDialog();
        }

        private void registrujToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormRegistracijaClana nova = new FormRegistracijaClana(bibl);
            nova.ShowDialog();
        }

        private void registrujKnjiguToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormRegistracijaKnjige registracijaaa = new FormRegistracijaKnjige(bibl);
            registracijaaa.ShowDialog();

        }

        private void obrišiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormObrisiClana nova = new FormObrisiClana(bibl);
            nova.ShowDialog();
        }

        private void obrišiKnjiguToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormObrisiKnjigu nova = new FormObrisiKnjigu(bibl);
            nova.ShowDialog();
        }

        private void dodajToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormDodajUposlenog nova = new FormDodajUposlenog(bibl);
            nova.ShowDialog();
        }

        private void obrišiUposlenogToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormObrisiUposlenog nova = new FormObrisiUposlenog(bibl);
            nova.ShowDialog();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void pretragaToolStripMenuItem_Click(object sender, EventArgs e)
        {
           
          

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void pretragaToolStripMenuItem3_Click(object sender, EventArgs e)
        {
            tabControl1.Visible = true;
        }

        private void tabPage1_Click(object sender, EventArgs e)
        {

        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            string pojam = textBox1.Text;
            try {
                if (radioButton1.Checked)
                {
                    richTextBox1.Clear();
                    int sifra = Convert.ToInt32(pojam);
                    Clan novi = bibl.PretraziClanaSaSifrom(sifra);
                    richTextBox1.Text = "Član sa šifrom " + sifra + " je pronađen.\n" + "Ime: " + novi.DajIme() + "\n" + "Prezime: " + novi.DajPrezime() + "\n" + "Username: " + novi.DajUsername();
                }
                else if (radioButton3.Checked)
                {
                    richTextBox1.Clear();
                    List<Clan> lista = bibl.PretraziClanaSaImenom(pojam);
                    for (int i = 0; i < lista.Count; i++)
                    {
                        richTextBox1.Text += lista[i].IspisiClana() + "\n" + "\n";
                    }
                }
                else if (radioButton2.Checked)
                {
                    richTextBox1.Clear();
                    List<Clan> lista = bibl.PretraziClanaSaPrezimenom(pojam);
                    for (int i = 0; i < lista.Count; i++)
                    {
                        richTextBox1.Text += lista[i].IspisiClana() + "\n" + "\n";
                    }
                }
                else if (radioButton4.Checked)
                {
                    richTextBox1.Clear();
                    Clan novi = bibl.PretraziClanaSaUsernemom(pojam);
                    richTextBox1.Text += novi.IspisiClana() + "\n";

                }
            }
            catch
            {
                toolStripStatusLabel1.Visible = true;
                toolStripStatusLabel1.Text = "Nije pronađen!";
            }
            }


        private void button2_Click(object sender, EventArgs e)
        {
            string pojam = textBox2.Text;
            try
            {
                if (radioButton5.Checked)
                {
                    richTextBox2.Clear();
                    List<PredmetiCitanja> lista = bibl.PretraziKnjiguSaImenom(pojam);
                    for (int i = 0; i < lista.Count; i++)
                    {
                        richTextBox2.Text += lista[i].IspisiKnjigu() + "\n" + "\n";
                    }

                }
                else if (radioButton6.Checked)
                {
                    richTextBox1.Clear();
                    List<PredmetiCitanja> lista = bibl.PretraziKnjiguSaAutorom(pojam);
                    for (int i = 0; i < lista.Count; i++)
                    {
                        richTextBox2.Text += lista[i].IspisiKnjigu() + "\n" + "\n";
                    }
                }
                else if (radioButton7.Checked)
                {
                    int sifra = Convert.ToInt32(pojam);
                    richTextBox1.Clear();
                    PredmetiCitanja novi = bibl.PretraziKnjiguSaSifrom(sifra);
                    richTextBox2.Text += novi.IspisiKnjigu() + "\n";
                }
                else if (radioButton8.Checked)
                {

                    richTextBox2.Clear();
                    PredmetiCitanja novi = bibl.PretraziKnjiguSaISBNSifrom(pojam);
                    richTextBox2.Text += novi.IspisiKnjigu() + "\n";

                }
            }
            catch
            {
                toolStripStatusLabel2.Visible = true;
                toolStripStatusLabel2.Text = "Nije pronađen!";
            }
        }

        private void FormAdmin_Paint(object sender, PaintEventArgs e)
        {
            
        }

        private void tabPage1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void radioButton5_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            string pojam = textBox3.Text;
            try
            {
                if (radioButton9.Checked)
                {
                    richTextBox3.Clear();
                    List<Uposleni> lista = bibl.PretraziUposlenogSaImenom(pojam);
                    for (int i = 0; i < lista.Count; i++)
                    {
                        richTextBox3.Text += lista[i].IspisiUposlenog() + "\n" + "\n";
                    }

                }

                else if (radioButton10.Checked)
                {
                    richTextBox3.Clear();
                    List<Uposleni> lista = bibl.PretraziUposlenogSaPrezimenom(pojam);
                    for (int i = 0; i < lista.Count; i++)
                    {
                        richTextBox3.Text += lista[i].IspisiUposlenog() + "\n" + "\n";
                    }
                }
                else if (radioButton11.Checked)
                {

                    richTextBox3.Clear();
                    int sifra = Convert.ToInt32(pojam);
                    Uposleni novi = bibl.PretraziUposlenogSaSifrom(sifra);
                    richTextBox3.Text = "Član sa šifrom " + sifra + " je pronađen.\n" + "Ime: " + novi.DajIme() + "\n" + "Prezime: " + novi.DajPrezime() + "\n" + "Username: " + novi.DajUsername();
                }
                else if (radioButton12.Checked)
                {

                    richTextBox3.Clear();
                   
                    Uposleni novi = bibl.PretraziUposlenogSaUsernameom(pojam);
                    richTextBox3.Text += novi.IspisiUposlenog() + "\n";

                }
            }
            catch
            {
                toolStripStatusLabel3.Visible = true;
                toolStripStatusLabel3.Text = "Nije pronađen!";

            }
            
            
        }

        private void finansijeToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void ažurirajToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Opcija ažuriranja u izradi", "Ažuriranje",MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
        }

        private void ažurirajToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Opcija ažuriranja u izradi", "Ažuriranje", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
        }

        private void ažurirajToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Opcija ažuriranja u izradi", "Ažuriranje", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
        }
    }
}
