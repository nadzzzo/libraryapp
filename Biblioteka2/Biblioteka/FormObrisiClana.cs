﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Biblioteka
{
    public partial class FormObrisiClana : Form
    {
        Biblioteka biblioteka;
        public FormObrisiClana(Biblioteka b)
        {
            InitializeComponent();
            biblioteka = b;
        }

        private void FormObrisiClana_Load(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            string user = textBox1.Text;
            try
            {
                biblioteka.ObrisiClanaSaUsernemom(user);
                toolStripStatusLabel1.Visible = true;
                toolStripStatusLabel1.Text = "Uspješno obrisan član!";
                toolStripStatusLabel1.ForeColor = Color.Green;
            }
            catch
            {
                toolStripStatusLabel1.Visible = true;
                toolStripStatusLabel1.Text = "Nije pronađen član sa tim username-om!";
                toolStripStatusLabel1.ForeColor = Color.Red;
            }
        }

        private void statusStrip2_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }
    }
}
