﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Biblioteka
{
    public partial class FormObrisiKnjigu : Form
    {
        Biblioteka biblioteka;
        public FormObrisiKnjigu(Biblioteka b)
        {
            biblioteka = b;
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int sifraknjige = Convert.ToInt32(textBox1.Text);
            try
            {
                biblioteka.ObrisiKnjiguSaSifrom(sifraknjige);
                toolStripStatusLabel1.Visible = true;
                toolStripStatusLabel1.Text = "Uspješno obrisana knjiga!";
                toolStripStatusLabel1.ForeColor = Color.Green;
            }
            catch
            {
                toolStripStatusLabel1.Visible = true;
                toolStripStatusLabel1.Text = "Nije pronađena knjiga sa tom šifrom!";
                toolStripStatusLabel1.ForeColor = Color.Red;

            }
            

        }
    }
}
