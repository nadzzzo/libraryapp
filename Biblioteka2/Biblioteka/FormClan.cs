﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Knjige;
using Osobe;

namespace Biblioteka
{
    public partial class FormClan : Form
    {
        Biblioteka bibl;
        Clan clan;
        string username;
        public FormClan(Biblioteka b,string user, Clan c)
        {
            InitializeComponent();
            bibl = b;
            username = user;
            clan = c;
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            label1.Text = "Dobro došao/la " + clan.DajIme() +" " + clan.DajPrezime() + "!";
            List<PredmetiCitanja> lista = bibl.DajSveKnjige();
            foreach (var K in lista)

                comboBox1.Items.Add(K.DajNaslovKnjige() + " - " + K.DajAutora());

        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void toolStripButton4_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            string s = textBox1.Text;
            checkedListBox1.Items.Add(s);
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            richTextBox2.Clear();
             richTextBox2.Visible = true;
            List<PredmetiCitanja> knjige = bibl.DajIznajmljeneKnjige(clan);
            
            for(int i = 0; i < knjige.Count(); i++)
            {
                richTextBox2.Text += knjige[i].DajNaslovKnjige() + " -  " + knjige[i].DajAutora() + "\n";
            }
           
        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {
        }

        private void button4_Click(object sender, EventArgs e)
        {
            richTextBox2.Clear();
            richTextBox2.Visible = true;
            DateTime datum = bibl.DatumIznajmljivanja(clan);
            datum.AddDays(7);
            int razlika = Convert.ToInt32( datum.Day - DateTime.Today.Day);
            if (razlika >= 0)
                richTextBox2.Text = "Broj preostalih dana je : " + razlika;
            else
                richTextBox2.Text = "Niste vratili knjigu na vrijeme!";


        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

       private void button3_Click(object sender, EventArgs e)
        {
            label8.Visible = true;
            textBox2.Visible = true;
            richTextBox2.Visible = true;
            string s = textBox2.Text;
            List<PredmetiCitanja> lista = bibl.PretragaKnjiga(s);
            foreach (var K in lista)
            {
                richTextBox2.Text += K.DajNaslovKnjige() + " - " + K.DajAutora() + " \n";
                    }

        }
        
        private void checkedListBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
           // checkedListBox1.Items.Remove(checkedListBox1.SelectedItem);

        }

        private void button5_Click(object sender, EventArgs e)
        {
            richTextBox2.Visible = true;
            richTextBox2.Clear();
            
            TimeSpan razlika  = (bibl.DatumUclanjenja(clan) - DateTime.Now);
            int razl = razlika.Days;
            richTextBox2.Text = "Preostalo je još " + razl + " dana do isteka Vaše članarine.";
        }

        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            richTextBox2.Clear();
            label8.Visible = true;
            textBox2.Visible = true;
            richTextBox2.Visible = true;
            string s = textBox2.Text;
            List<PredmetiCitanja> lista = bibl.PretraziKnjige(s);
            foreach (var K in lista)
            { 
                richTextBox2.Text += K.DajNaslovKnjige() + " - " + K.DajAutora() + " \n";
            }
            if (s == "") richTextBox2.Clear();
                     
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void button3_Click_1(object sender, EventArgs e)
            
        {
            try
            {
                string naziv = comboBox1.SelectedItem.ToString();
                PredmetiCitanja knjiga = bibl.NadjiKnjiguSaImenom(naziv);
                bibl.IznajmiKnjiguClanu(knjiga, clan, DateTime.Now);
                toolStripStatusLabel1.Visible = true;
                toolStripStatusLabel1.Text = "Uspješno iznajmljena knjiga!";
            }
            catch
            {
                toolStripStatusLabel1.Visible = true;
                toolStripStatusLabel1.Text = "Niste odabrali knjigu!"; 
            }

            

            
        }

        private void obrišiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            checkedListBox1.Items.Remove(checkedListBox1.SelectedItem);
        }
    }
}
