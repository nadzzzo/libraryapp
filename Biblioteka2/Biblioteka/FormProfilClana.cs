﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Osobe;
using Knjige;

namespace Biblioteka
{
    public partial class FormProfilClana : Form
    {
        Biblioteka bibl;
        Clan clan;

        public FormProfilClana(Biblioteka b, Clan p)
        {
            InitializeComponent();
            bibl = b;
            clan = p;
        }

        private void Form5_Load(object sender, EventArgs e)
        {
            this.Text = clan.DajUsername();
            textBox1.Text = clan.DajIme();
            textBox2.Text = clan.DajPrezime();



        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            clan.PostaviKomentarClanu(richTextBox1.Text);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            richTextBox2.Visible = true;
            richTextBox2.Text = clan.DajKomentar();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {

            List<PredmetiCitanja> iznajmljene = bibl.DajIznajmljeneKnjige(clan);
           
            for (int i = 0; i < iznajmljene.Count; i++)
            {
                treeView1.Nodes.Add(iznajmljene[i].DajNaslovKnjige() + " - " + iznajmljene[i].DajAutora());
                treeView1.Nodes[i].Nodes.Add(iznajmljene[i].DajSifruKnjige().ToString());

            }
        }
    }
}
