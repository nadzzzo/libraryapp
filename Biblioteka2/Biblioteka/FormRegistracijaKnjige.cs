﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Biblioteka;
using Knjige;

namespace Biblioteka
{
    public partial class FormRegistracijaKnjige : Form
    {
        Biblioteka bibl;
        public FormRegistracijaKnjige(Biblioteka b)
        {
            InitializeComponent();
            bibl = b;
        }

        private void FormRegistracijaKnjige_Load(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void maskedTextBox1_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void maskedTextBox1_MaskInputRejected_1(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void label11_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            
            string naziv = textBox1.Text;
            string autor = textBox5.Text;
            string zanr = textBox2.Text;
            int sifra = Convert.ToInt32(textBox3.Text);
            string isbn = maskedTextBox2.Text;
            int godinaizdanja = Convert.ToInt32(numericUpDown1.Value);
            string izdavac = textBox4.Text;
            try
            {
                if (textBox1.Text == "" || textBox2.Text == "" || textBox3.Text == "" || textBox4.Text == "" || textBox5.Text == "" || maskedTextBox2.Text == "")
                {
                    throw new Exception();
                }
                if (radioButton1.Checked)
                {
                    ObicneKnjige knjiga = new ObicneKnjige(sifra, naziv, autor, zanr, izdavac, godinaizdanja, isbn);
                    bibl.RegistrujKnjigu(knjiga);
                    toolStripStatusLabel1.Visible = true;
                    toolStripStatusLabel1.Text = "Uspješno ste registrovali knjigu!";
                    toolStripStatusLabel1.ForeColor = Color.Green;
                }
                else if (radioButton2.Checked)
                {
                    string imeautorskekuce = textBox6.Text;
                    List<string> spisakumjetnika = new List<string>();
                    spisakumjetnika.Add(textBox7.Text);
                    int brojizdanja = Convert.ToInt32(numericUpDown2.Value);
                    bool obicno = false;
                    if (radioButton4.Checked) { obicno = true; }
                    else if (radioButton5.Checked) { obicno = false; }

                    Stripovi knjiga = new Stripovi(sifra, naziv, autor, zanr, izdavac, godinaizdanja, isbn, imeautorskekuce, spisakumjetnika, brojizdanja, obicno);
                    bibl.RegistrujKnjigu(knjiga);
                    toolStripStatusLabel1.Visible = true;
                    toolStripStatusLabel1.Text = "Uspješno ste registrovali knjigu!";
                    toolStripStatusLabel1.ForeColor = Color.Green;
                }
                else if (radioButton3.Checked)
                {
                    string konferencija = textBox8.Text;
                    string oblastnauke = textBox9.Text;

                    NaucniRadovi knjiga = new NaucniRadovi(sifra, naziv, autor, zanr, izdavac, godinaizdanja, isbn, konferencija, oblastnauke);
                    bibl.RegistrujKnjigu(knjiga);
                    toolStripStatusLabel1.Visible = true;
                    toolStripStatusLabel1.Text = "Uspješno ste registrovali knjigu!";
                    toolStripStatusLabel1.ForeColor = Color.Green;
                }
            }
            catch
            {
                toolStripStatusLabel1.Visible = true;
                toolStripStatusLabel1.Text = "Registracija nije uspjela!";
                
            }
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton1.Checked)
            {
                groupBox4.Visible = false;
                panel1.Visible = false;
                groupBox3.Visible = false;
            }
            else if (radioButton2.Checked)
            {
                panel1.Visible = false;
                groupBox3.Visible = true;
                groupBox4.Visible = false;
            }
            else if (radioButton3.Checked)
            {
                groupBox4.Visible = true;
                groupBox3.Visible = false;
                panel1.Visible = true;
            }

        }

        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton1.Checked)
            {
                groupBox4.Visible = false;
                panel1.Visible = false;
                groupBox3.Visible = false;
            }
            else if (radioButton2.Checked)
            {
                panel1.Visible = false;
                groupBox3.Visible = true;
                groupBox4.Visible = false;
            }
            else if (radioButton3.Checked)
            {
                groupBox4.Visible = true;
                groupBox3.Visible = false;
                panel1.Visible = true;
            }

        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
          if(radioButton1.Checked)
            {
                groupBox4.Visible = false;
                panel1.Visible = false;
                groupBox3.Visible = false;
            }
          else if(radioButton2.Checked)
            {
                panel1.Visible = false;
                groupBox3.Visible = true;
                groupBox4.Visible = false;
            }
          else if(radioButton3.Checked)
            {
                groupBox4.Visible = true;
                groupBox3.Visible = false;
                panel1.Visible = true;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void textBox1_Validated(object sender, EventArgs e)
        {
            if (textBox1.Text == "") errorProvider1.SetError(this.textBox1, "Nije unesen naziv");
            else errorProvider1.SetError(this.textBox1, String.Empty);
        }

        private void textBox5_Validated(object sender, EventArgs e)
        {
            if (textBox5.Text == "") errorProvider2.SetError(this.textBox5, "Nije unesen autor");
            else errorProvider2.SetError(this.textBox5, String.Empty);
        }

        private void textBox2_Validated(object sender, EventArgs e)
        {
            if (textBox2.Text == "") errorProvider3.SetError(this.textBox2, "Nije unesen žanr");
            else errorProvider3.SetError(this.textBox2, String.Empty);
        }

        private void textBox3_Validated(object sender, EventArgs e)
        {
            if (textBox3.Text == "") errorProvider4.SetError(this.textBox3, "Nije unesena šifra");
            else errorProvider4.SetError(this.textBox3, String.Empty);
        }

        private void textBox4_Validated(object sender, EventArgs e)
        {
            if (textBox4.Text == "") errorProvider5.SetError(this.textBox4, "Nije unesen izdavač");
            else errorProvider5.SetError(this.textBox4, String.Empty);
        }

        private void maskedTextBox1_Validated(object sender, EventArgs e)
        {
            if (maskedTextBox1.Text == "ISBN    - -  -      -") errorProvider6.SetError(this.maskedTextBox1, "Nije unesen ISBN");
            else errorProvider6.SetError(this.maskedTextBox1, String.Empty);
        }

        private void maskedTextBox2_Validated(object sender, EventArgs e)
        {
            if (maskedTextBox2.Text == "ISBN  -      -  -") errorProvider7.SetError(this.maskedTextBox2, "Nije unesen ISBN");
            else errorProvider7.SetError(this.maskedTextBox2, String.Empty);
        }

        private void textBox8_Validated(object sender, EventArgs e)
        {
            if (radioButton3.Checked)
            {
                if (textBox8.Text == "") errorProvider8.SetError(this.textBox8, "Nije unesena konferencija");
                else errorProvider8.SetError(this.textBox8, String.Empty);
            }
        }

        private void textBox9_Validated(object sender, EventArgs e)
        {
            if (radioButton3.Checked)
            {
                if (textBox9.Text == "") errorProvider9.SetError(this.textBox9, "Nije unesena oblast nauke");
                else errorProvider9.SetError(this.textBox9, String.Empty);
            }
        }

        private void textBox6_Validated(object sender, EventArgs e)
        {
            if (radioButton2.Checked)
            {
                if (textBox6.Text == "") errorProvider10.SetError(this.textBox6, "Nije uneseno ime autorske kuće");
                else errorProvider10.SetError(this.textBox6, String.Empty);
            }
        }

        private void textBox7_Validated(object sender, EventArgs e)
        {
            if (radioButton3.Checked)
            {
                if (textBox7.Text == "") errorProvider11.SetError(this.textBox7, "Nije unesen spisak umjetnika");
                else errorProvider11.SetError(this.textBox7, String.Empty);
            }
        }

        private void statusStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }
    }
}
