﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Osobe;
using Knjige;


namespace Biblioteka
{
    public partial class FormPocetna : Form
    {
        Biblioteka b;
       
        public FormPocetna(Biblioteka k)
        {
            InitializeComponent();
            b = k;
          

        }

        private void Pocetna_Load(object sender, EventArgs e)
        {
            //label3.Text = "Dobro došli u biblioteku 17601";
        }
        private void button1_Click(object sender, EventArgs e)
        {
            string user = textBox1.Text;
            string pass = textBox2.Text;
            string pravi = b.CalculateMD5Hash(pass);

           
                bool postoji = false;
                try
                {
                    Clan clan = b.NadjiClana(user);
                    string studentov = clan.DajPassword();
                    if (studentov == pravi)

                    {
                        postoji = true;
                        FormClan clanskaforma = new FormClan(b, user, clan);
                        clanskaforma.ShowDialog();
                    }
                }
                catch (Exception a)
                {
                    
                }

                try
                {

                    Uposleni uposlenik = b.NadjiUposlenog(user);
                    string bibliotekarov = uposlenik.DajPassword();
                    if (bibliotekarov == pravi && b.DaLiJeAdmin(user, pravi) == false)
                    {
                         postoji = true;
                        FormBibliotekar bibliotekarskaforma = new FormBibliotekar(b, user, uposlenik);
                        bibliotekarskaforma.ShowDialog();
                    }
                }
                catch (Exception a)
                {
                   
                }

                try
                {
                    bool admin = b.DaLiJeAdmin(user, pravi);

                    if (admin == true)
                    {
                         postoji = true;
                        FormAdmin adminforma = new FormAdmin(b);
                        adminforma.ShowDialog();
                    }
                }
                catch (Exception)
                {

                }
                if(postoji == false)
            {
                toolStripStatusLabel1.Visible = true;
            }
           
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {
            
        }

        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
        {

        }

        private void pomoćToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void adresaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            richTextBox1.Clear();
            richTextBox1.Text = "Biblioteka 17601 Elektrotehničkog fakulteta Sarajevo \nZmaja od Bosne bb \nKampus Univerziteta \n71000 Sarajevo ";
           
        }

        private void telefonToolStripMenuItem_Click(object sender, EventArgs e)
        {
            richTextBox1.Clear();
            richTextBox1.Text = "Kontakt telefon: +387 33/590-573 \n                              +387 33/270-583";
        }

        private void emailToolStripMenuItem_Click(object sender, EventArgs e)
        {
            richTextBox1.Clear();
            richTextBox1.Text = "E-mail adresa :\nnzilic1@etf.unsa.ba\nbiblioteka_17601@etf.unsa.ba\nnadja.zilic1311@gmail.com";
        }

        private void radnoVrijemeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            richTextBox1.Clear();
            richTextBox1.Text = "Radno vrijeme biblioteke :\n Svaki radni dan od 08:00 do 16:00";
        }

        private void članToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void donacijeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            richTextBox1.Clear();
            richTextBox1.Text = "Ukoliko želite pomoći radu naše biblioteke, u vidu donacije knjiga obratite se na kontakt mail nzilic1@etf.unsa.ba. \nUkoliko želite pomoći radu naše biblioteke, u vidu uplate novca žiro račun je : 1610000003210032";
           
        }

        private void listaŽeljaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            richTextBox1.Clear();
            richTextBox1.Text = "Na jednostavan način možete dodavati knjige koje želite pročitati pritiskom na dugme Dodaj.\nUkoliko pročitate određenu knjigu, čekirajte je i ona će nestati iz Vaše liste. ";
        }

        private void iznajmljeneKnjigeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            richTextBox1.Clear();
            richTextBox1.Text = "Pritiskom na dugme Prikaži imat ćete uvid u knjige koje se trenutno nalaze kod Vas. ";
        }

        private void istekRokaČlanarineToolStripMenuItem_Click(object sender, EventArgs e)
        {
            richTextBox1.Clear();
            richTextBox1.Text = "Pritiskom na dugme Prikaži imat ćete uvid u preostali broj dana važenja Vaše članarine. ";
        }

        private void istekRokaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            richTextBox1.Clear();
            richTextBox1.Text = "Pritiskom na dugme Prikaži imat ćete uvid u broj dana preostalih da vratite knjigu. ";
        }

        private void pretragaKnjigaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            richTextBox1.Clear();
            richTextBox1.Text = "Unosom naslova u polje Pretraga knjige, jednostavno  možete pretraživati knjige koje se nalaze u biblioteci. ";
        }

        private void prikazSvihKnjigaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            richTextBox1.Clear();
            richTextBox1.Text = "Pritiskom na dugme Prikaži, dobijate uvid u bazu biblioteke. ";
        }

        private void provjeraStanjaKnjigeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            richTextBox1.Clear();
            richTextBox1.Text = "U polje Stanje knjige unesite naziv knjige, te pritiskom na dugme Provjeri ćete dobiti uvid da li je knjiga dostupna.";
        }

        private void iznajmljivanjeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            richTextBox1.Clear();
            richTextBox1.Text = "U polje Knjiga unesite šifru knjige.\nU polje Član unesite username člana.\nU polje datum odaberite datum dana iznajmljivanja.\nPritiskom na dugme Iznajmi, izvršit ćete iznajmljivanje knjige ukoliko je to moguće.";
        }

        private void pretragaČlanovaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            richTextBox1.Clear();
            richTextBox1.Text = "U polje Username unesite username člana kojeg želite pronaći, a zatim će se pritiskom na dugme Pretraži prikazati profil traženog člana.";
        }

        private void promjenaStanjaKnjigeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            richTextBox1.Clear();
            richTextBox1.Text = "U polje Dodaj na stanje unesite šifru knjige kojoj mijenjate stanje te izaberite broj.\nPritiskom na dugme OK broj primjeraka knjige će se promijeniti.";
        }

        private void textBox1_Validated(object sender, EventArgs e)
        {
            if (textBox1.Text == "") errorProvider1.SetError(this.textBox1, "Nije uneseno ime");
            else errorProvider1.SetError(this.textBox1, String.Empty);
        }

        private void textBox2_Validated(object sender, EventArgs e)
        {
            if (textBox2.Text == "") errorProvider2.SetError(this.textBox2, "Nije unesen password");
            else errorProvider2.SetError(this.textBox2, String.Empty);
        }

        private void tabPage1_Click(object sender, EventArgs e)
        {

        }

        private void tabPage1_Paint(object sender, PaintEventArgs e)      
        {
           
            Pen mojPen = new Pen(System.Drawing.Color.Blue, 8);
            SolidBrush mojBrush = new SolidBrush(System.Drawing.Color.White);
            Point[] polygonTacke = new Point[3];
            polygonTacke[0] = new Point(100, 140);
            polygonTacke[1] = new Point(170, 40);
            polygonTacke[2] = new Point(240, 140);

            e.Graphics.DrawRectangle(mojPen, 150, 20, 40, 10);
            e.Graphics.DrawPolygon(mojPen, polygonTacke);
            e.Graphics.DrawLines(mojPen, polygonTacke);
            e.Graphics.FillPolygon(mojBrush, polygonTacke);
            e.Graphics.DrawString("ETF Biblioteka\n     17601", new Font("Times New Roman", 12, FontStyle.Regular), new SolidBrush(Color.Blue), 120, 100);


        }

        private void button1_Validated(object sender, EventArgs e)
        {

        }
    }
}
