﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Biblioteka
{
    public partial class FormObrisiUposlenog : Form
    {
        Biblioteka biblioteka;
        public FormObrisiUposlenog(Biblioteka b)
        {
            InitializeComponent();
            biblioteka = b;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FormObrisiUposlenog_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            int sifrauposlenog = Convert.ToInt32(textBox1.Text);

            try
            {
                biblioteka.ObrisiUposlenogSaSifrom(sifrauposlenog);
                toolStripStatusLabel1.Visible = true;
                toolStripStatusLabel1.Text = "Uspješno obrisan uposlenik!";
                toolStripStatusLabel1.ForeColor = Color.Green;
            }
            catch
            {
                toolStripStatusLabel1.Visible = true;
                toolStripStatusLabel1.Text = "Nije pronađen zaposlenik sa tom šifrom!";
                toolStripStatusLabel1.ForeColor = Color.Red;
            }
        }
    }
}