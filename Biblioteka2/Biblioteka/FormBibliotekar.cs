﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Osobe;
using Knjige;
namespace Biblioteka
{
    public partial class FormBibliotekar : Form
    {
        Biblioteka bibl;
        string username;
        Uposleni uposlenik;
        public FormBibliotekar(Biblioteka b, string user, Uposleni p)
        {
            InitializeComponent();
            bibl = b;
            username = user;
            uposlenik = p;
        }

        private void Form3_Load(object sender, EventArgs e)
        {
            label1.Text = "Dobro došao/la " + uposlenik.DajIme() + " " + uposlenik.DajPrezime() + "!";
        }

        private void button1_Click(object sender, EventArgs e)
        {

            
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            listBox1.Items.Clear();
            listBox1.Visible = true;
            int i = 0;
            foreach (var K in bibl.DajSveKnjige())
            {
                listBox1.Items.Insert(i, K.DajNaslovKnjige() + "  - " + K.DajAutora() + " - " + K.DajBrojPrimjeraka() );
                i++;
                    }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string s = textBox1.Text;
           
            listBox1.Items.Clear();
            bool dostupna = false;
            foreach(var K in bibl.DajSveKnjige())
             {
                if(K.DajNaslovKnjige() == s)
                {
                    if (K.DaLiJeIzdata() == false)
                        dostupna = true;
                }
            }


            if (dostupna == true)
            {
                listBox1.Items.Insert(0, "Tražena knjiga je dostupna.");
                listBox1.Visible = true;
            }
            else if (dostupna == false && textBox1.Text != "")
            {
                listBox1.Visible = true;
                listBox1.Items.Insert(0, "Tražena knjiga nije dostupna.");
            }
            else if (textBox1.Text == "")
            {
                toolStripStatusLabel1.Visible = true;
                toolStripStatusLabel1.Text = "Niste unijeli pojam!";
            }
           
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            
            try
            {
                int knjiga = Convert.ToInt32(textBox2.Text);
                PredmetiCitanja trazena = bibl.NadjiKnjiguSaSifrom(knjiga);
                string clan = textBox3.Text;
                Clan trazeni = bibl.NadjiClana(clan);
                DateTime datum = dateTimePicker1.Value;
                bibl.IznajmiKnjiguClanu(trazena, trazeni, datum);
                toolStripStatusLabel1.Visible = true;
                toolStripStatusLabel1.Text = "Uspješno ste iznajmili knjigu " + trazena.DajNaslovKnjige() + " - " + trazena.DajAutora() + " članu " + trazeni.DajIme() + " " + trazeni.DajPrezime();
                    
            }
            catch(Exception izuzetak)
            {
                toolStripStatusLabel1.Visible = true;
                toolStripStatusLabel1.Text = "Iznajmljivanje nije uspjelo!";

            }


        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            try
            {   string username = textBox4.Text;
                Clan clan = bibl.NadjiClana(username);
                FormProfilClana clanskaforma = new FormProfilClana(bibl, clan);
                clanskaforma.ShowDialog();
            }
            catch(Exception izuzetak)
            {
                toolStripStatusLabel1.Visible = true;
                toolStripStatusLabel1.Text = "Nije pronađen korisnik sa tim username-om!";

            }

        }

        private void button5_Click(object sender, EventArgs e)
        {
            try
            {

                int sifra = Convert.ToInt32(textBox5.Text);

                int brprimjeraka = Convert.ToInt32(numericUpDown1.Value);
                bibl.PromijeniStanjeKnjizi(sifra, brprimjeraka);

            }
            catch
            {
                
                    toolStripStatusLabel1.Visible = true;
                    toolStripStatusLabel1.Text = "Niste unijeli šifru knjige!";
                
            }
         
            


        }

        private void button2_Validated(object sender, EventArgs e)
        {
            if (textBox1.Text == "") errorProvider1.SetError(this.textBox1, "Unesite pojam");
            else errorProvider1.SetError(this.textBox1, String.Empty);
        }

        private void textBox1_Validated(object sender, EventArgs e)
        {
           /* if (textBox1.Text == "" && button2.) errorProvider1.SetError(this.textBox1, "Nije unesen pojam");
            else errorProvider1.SetError(this.textBox1, String.Empty);*/
        }

        private void textBox4_Validated(object sender, EventArgs e)
        {
          
        }

        private void button6_Click(object sender, EventArgs e)
        {
            string user = textBox7.Text;
            int sifra = Convert.ToInt32(textBox6.Text);
            try
            {
                bibl.VratiKnjigu(sifra, user);
                toolStripStatusLabel1.Visible = true;
                toolStripStatusLabel1.Text = "Uspješno vraćena knjiga!";

            }
            catch
            {
                toolStripStatusLabel1.Visible = true;
                toolStripStatusLabel1.Text = "Nije moguće vratiti knjigu!";
            }

        }
    }
}
