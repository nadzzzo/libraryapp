﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Osobe;

namespace Biblioteka
{
    public partial class FormRegistracijaClana : Form
    {
        Biblioteka biblioteka;
        public FormRegistracijaClana(Biblioteka b)
        {
            InitializeComponent();
            biblioteka = b;
        }

        private void FormRegistracijaClana_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            string ime = textBox1.Text;
            string prezime = textBox2.Text;
            DateTime datumrodjenja = dateTimePicker1.Value;
            string maticni = maskedTextBox1.Text;
            string username = textBox3.Text;
            string pass = textBox4.Text;
            int sifra = Convert.ToInt32(textBox5.Text);
            bool mjesecna = false, godisnja = false;
            if (radioButton3.Checked) { mjesecna = true; godisnja = false; }
            else if (radioButton4.Checked) { mjesecna = false; godisnja = true; }
            DateTime datumuclanjenja = dateTimePicker2.Value;
              
            try
            {

                if (textBox1.Text == "" || textBox2.Text == "" || textBox3.Text == "" || textBox4.Text == "" || textBox5.Text == "" || maskedTextBox1.Text == "")
                {
                    throw new Exception();
                }
               if (radioButton5.Checked)
                {
                    ObicniClan clan = new ObicniClan(ime, prezime, maticni, datumrodjenja, sifra, mjesecna, godisnja, "", datumuclanjenja, username, pass);
                    biblioteka.RegistrujClana(clan, datumuclanjenja);

                    toolStripStatusLabel1.Visible = true;
                    toolStripStatusLabel1.Text = "Uspješno ste registrovali člana!";
                }
                else if (radioButton8.Checked)
                {
                    groupBox5.Visible = true;
                    label10.Visible = true;
                    textBox6.Visible = true;
                    label10.Text = "Šifra profesora: ";
                    int sifraprof = Convert.ToInt32(textBox6.Text);
                    Profesori clan = new Profesori(ime, prezime, maticni, datumrodjenja, sifra, mjesecna, godisnja, "", datumuclanjenja, sifraprof, username, pass);
                    biblioteka.RegistrujClana(clan, datumuclanjenja);
                    toolStripStatusLabel1.Visible = true;
                    toolStripStatusLabel1.Text = "Uspješno ste registrovali člana!";

                }
                else if (radioButton6.Checked)
                {
                    groupBox5.Visible = true;
                    label10.Visible = true;
                    textBox6.Visible = true;
                    label10.Text = "Indeks studenta: ";
                    int indeks = Convert.ToInt32(textBox6.Text);
                    BoE clan = new BoE(ime, prezime, maticni, datumrodjenja, sifra, mjesecna, godisnja, "", datumuclanjenja, indeks, username, pass);
                    biblioteka.RegistrujClana(clan, datumuclanjenja);
                    toolStripStatusLabel1.Visible = true;
                    toolStripStatusLabel1.Text = "Uspješno ste registrovali člana!";
                }
                else if (radioButton7.Checked)
                {

                    int indeks = Convert.ToInt32(textBox6.Text);
                    MoE clan = new MoE(ime, prezime, maticni, datumrodjenja, sifra, mjesecna, godisnja, "", datumuclanjenja, indeks, username, pass);
                    biblioteka.RegistrujClana(clan, datumuclanjenja);
                    toolStripStatusLabel1.Visible = true;
                    toolStripStatusLabel1.Text = "Uspješno ste registrovali člana!";
                }
            }
            catch
            {
                toolStripStatusLabel1.Text = "Registracija nije uspjela!";

            }
         }

        private void label10_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void radioButton5_CheckedChanged(object sender, EventArgs e)
        {
            if(radioButton5.Checked)
            {
                groupBox5.Visible = false;
            }
            else if(radioButton8.Checked)
            {
                groupBox5.Visible = true;
                label10.Text = "Šifra profesora: ";
            }
            else if (radioButton6.Checked)
            {
                groupBox5.Visible = true;
                label10.Text = "Indeks studenta: ";
            }
            else if (radioButton7.Checked)
            {
                groupBox5.Visible = true;
                label10.Text = "Indeks studenta: ";
            }


        }

        private void radioButton7_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton5.Checked)
            {
                groupBox5.Visible = false;
            }
            else if (radioButton8.Checked)
            {
                groupBox5.Visible = true;
                label10.Text = "Šifra profesora: ";
            }
            else if (radioButton6.Checked)
            {
                groupBox5.Visible = true;
                label10.Text = "Indeks studenta: ";
            }
            else if (radioButton7.Checked)
            {
                groupBox5.Visible = true;
                label10.Text = "Indeks studenta: ";
            }
        }

        private void radioButton8_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton5.Checked)
            {
                groupBox5.Visible = false;
            }
            else if (radioButton8.Checked)
            {
                groupBox5.Visible = true;
                label10.Text = "Šifra profesora: ";
            }
            else if (radioButton6.Checked)
            {
                groupBox5.Visible = true;
                label10.Text = "Indeks studenta: ";
            }
            else if (radioButton7.Checked)
            {
                groupBox5.Visible = true;
                label10.Text = "Indeks studenta: ";
            }
        }

        private void radioButton6_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton5.Checked)
            {
                groupBox5.Visible = false;
            }
            else if (radioButton8.Checked)
            {
                groupBox5.Visible = true;
                label10.Text = "Šifra profesora: ";
            }
            else if (radioButton6.Checked)
            {
                groupBox5.Visible = true;
                label10.Text = "Indeks studenta: ";
            }
            else if (radioButton7.Checked)
            {
                groupBox5.Visible = true;
                label10.Text = "Indeks studenta: ";
            }
        }

        private void textBox1_Validated(object sender, EventArgs e)
        {
            if (textBox1.Text == "") errorProvider1.SetError(this.textBox1, "Nije uneseno ime");
            else errorProvider1.SetError(this.textBox1, String.Empty);
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox2_Validated(object sender, EventArgs e)
        {
            if (textBox2.Text == "") errorProvider2.SetError(this.textBox2, "Nije uneseno prezime");
            else errorProvider2.SetError(this.textBox2, String.Empty);
        }

        private void maskedTextBox1_Validated(object sender, EventArgs e)
        {
            errorProvider3.Clear();
            if(maskedTextBox1.Text == "" || biblioteka.ValidacijaMaticnog(maskedTextBox1.Text, dateTimePicker1.Value) == false)

            {
                errorProvider3.SetError(this.maskedTextBox1, "Neispravan matični broj!");
               
            }
            else errorProvider3.SetError(this.textBox1, String.Empty);
        }

        private void textBox3_Validated(object sender, EventArgs e)
        {
            if (textBox3.Text == "") errorProvider4.SetError(this.textBox3, "Nije unesen username!");
            else errorProvider4.SetError(this.textBox3, String.Empty);
        }

        private void textBox4_Validated(object sender, EventArgs e)
        {
            if (textBox4.Text == "") errorProvider5.SetError(this.textBox4, "Nije unesen password!");
            else errorProvider5.SetError(this.textBox4, String.Empty);

        }

        private void textBox7_Validated(object sender, EventArgs e)
        {
            if (textBox7.Text == "" )
                errorProvider6.SetError(this.textBox7, "Nije unesen password!");
            else if(textBox7.Text != "" && textBox7.Text != textBox4.Text)
                errorProvider6.SetError(this.textBox7, "Password se ne poklapa!");
            else errorProvider6.SetError(this.textBox7, String.Empty);

        }

        private void textBox5_Validated(object sender, EventArgs e)
        {
            if (textBox5.Text == "") errorProvider7.SetError(this.textBox5, "Nije unesena šifra!");
            else errorProvider7.SetError(this.textBox5, String.Empty);
        }

        private void textBox6_Validated(object sender, EventArgs e)
        {
            if(radioButton6.Checked || radioButton7.Checked || radioButton8.Checked)
            {
                if (textBox6.Text == "") errorProvider8.SetError(this.textBox6, "Nije unesena šifra/indeks!");
                else errorProvider8.SetError(this.textBox6, String.Empty);
            }
        }
    }
}
